import java.util.stream.IntStream;

public class SumThread extends Thread {
    private final int from;
    private final int to;
    private final int numberOfThread;

    public SumThread(int from, int to, int numberOfThread) {
        this.from = from;
        this.to = to;
        this.numberOfThread = numberOfThread;
    }

    @Override
    public void run() {
        IntStream.range(from, to).forEach(index-> Main.sums[numberOfThread] += Main.array[index]);
    }
}
