

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Реализовать многопоточное суммирование элементов
 */
public class Main {

    public static int[] array;

    public static int[] sums;

    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        int numbersCount = getCorrectNumberFromStream(scanner);
        int threadsCount = getCorrectNumberFromStream(scanner);

        array = SumThreadService.generateRandomArray(100, numbersCount);
        sums = new int[threadsCount];

        System.out.println("Сумма без потоков: " + SumThreadService.getSum(array));

        SumThreadService.calculateSumsOfArray(array, threadsCount);

        System.out.println("Сумма с потоками: " + SumThreadService.getSum(sums));
    }


    // получить из сканера корректное положительное целое число
    public static int getCorrectNumberFromStream(Scanner scanner) {
        while (true) {
            while (!scanner.hasNextInt()) {
                System.out.println("Введите целое число от 1 до 2 147 483 647");
                scanner.next();
            }
            int number = scanner.nextInt();
            if(number < 1) {
                System.out.println("Введите целое число от 1 до 2 147 483 647");
                scanner.next();
            } else {
                return number;
            }
        }

    }

}
