import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * Сервис для реализации многопоточного сложения чисел в массиве
 */
public class SumThreadService {

    /**
     * Возвращает шаг (количество элементов для обработки одним потоком)
     * @param array массив для которого будет рассчитан шаг
     * @param threadCount количество потоков
     */
    public static int getStep(int[] array, int threadCount) {
        return array.length % threadCount == 0 ? array.length / threadCount : array.length / (threadCount + 1);
    }

    public static int getSum(int[] array) {
        return Arrays.stream(array).sum();
    }

    /**
     * Вычислить промежуточные суммы массива array и записать их в {@link Main#sums}
     * @param array массив для которого нужно вычислить промежуточные суммы
     * @param threadsCount количество потоков для вычисления количества промежуточных сумм
     */
    public static void calculateSumsOfArray(int[] array, int threadsCount) {
        ArrayList<Thread> threads = new ArrayList<>();
        int step = SumThreadService.getStep(array, threadsCount);
        for(int threadNumber = 0; threadNumber < threadsCount; threadNumber++) {
            SumThread sumThread = (threadNumber == (threadsCount - 1)) && ((array.length % threadsCount) != 0) ?
                    new SumThread(threadNumber * step, array.length, threadNumber) :
                    new SumThread(threadNumber * step, threadNumber * step + step, threadNumber);

            sumThread.start();
            threads.add(sumThread);
        }
        // дожидаемся завершения всех потоков после того как у всех них был вызван start => параллельное выполнение
        joinAllThreads(threads);
    }

    private static void joinAllThreads(List<Thread> threads) {
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new IllegalArgumentException();
            }
        }
    }

    public static int[] generateRandomArray(int bound, int arrayLength) {
        Random random = new Random();
        int[] arrayWithRandomInts = new int[arrayLength];
        // заполняем случайными числами
        IntStream.range(0, arrayWithRandomInts.length)
                .forEach(index -> arrayWithRandomInts[index] = random.nextInt(bound));
        return arrayWithRandomInts;
    }
}
