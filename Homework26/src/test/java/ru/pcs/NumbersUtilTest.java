package ru.pcs;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.*;

@DisplayName(value = "NumbersUtil is working when")
@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
class NumbersUtilTest {

    private final NumbersUtil numbersUtil = new NumbersUtil();

    @Nested
    @DisplayName("gcd() is working")
    public class ForGcd {

        @ParameterizedTest(name = "return {2} on gcd({0}, {1})")
        @CsvSource(value = {"2, 10, 2", "13, 26, 13", "36, 60, 12"})
        public void on_not_equal_params_return_correct_nod(int firstNumber, int secondNumber, int result) {
            assertEquals(result, numbersUtil.gcd(firstNumber, secondNumber));
        }

        @ParameterizedTest(name = "return {2} on gcd({0}, {1})")
        @CsvSource(value = {"5, 5, 5", "6, 6, 6", "13, 13, 13"})
        public void on_equals_params_return_this_param(int firstNumber, int secondNumber, int result) {
            assertEquals(result, numbersUtil.gcd(firstNumber, secondNumber));
        }


        @ParameterizedTest(name = "throw exception on {0} and {1}")
        @CsvSource(value = {"-1, -2", "-6, 4", "5, -8"})
        public void bad_numbers_throws_exception(int firstNumber,int secondNumber) {
            assertThrows(IllegalArgumentException.class, () -> numbersUtil.gcd(firstNumber, secondNumber));
        }

    }

}