package ru.pcs;

public class NumbersUtil {
    // наибольший общий делитель
    public int gcd(int firstNumber, int secondNumber) {
        if(firstNumber < 0 || secondNumber < 0) {
            throw new IllegalArgumentException();
        }
        while (firstNumber != secondNumber) {
            if(firstNumber > secondNumber) {
                int tmp = firstNumber;
                firstNumber = secondNumber;
                secondNumber = tmp;
            }
            secondNumber -= firstNumber;
        }
        return firstNumber;
    }
}
